﻿# Контроль версии в работе Matlab и Simulink #

Здесь буду периодически описывать как вести контроль версии при работе в среде Matlab и Simulink.

## *1 Установка Git для работы с Bitbuket* ##

1.1 В первую очередь настроим наш Git – зададим своё имя и email:
```
git config --global user.name "rashid_s"
git config --global user.email rashid_s@inbox.ru
git config --global core.pager 'less -r'
```

## How do I get set up? ##

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

## Contribution guidelines ##

* Writing tests
* Code review
* Other guidelines

## Контактные данные ##

* Repo owner or admin
* Other community or team contact
